import os
import sys
import argparse

all_files_list = []

def find(word):
    def _find(path):
        with open(path, "rb") as fp:
            for n, line in enumerate(fp):
                if word in line:
                    yield n+1, line
    return _find

def search(source_folder, target_folder, source_extensions, target_extensions):
    for root, dirs, files in os.walk(source_folder):
        for file in files:
            for extension in source_extensions:
                if file.endswith(extension):
                    path = os.path.join(root, file)
                    for source_file in list_files(target_folder, target_extensions):
                        global all_files_list
                        all_files_list.append(source_file)
                        finder = find(source_file)
                        for line_number, line in finder(path):
                            yield source_file, path, line_number, line.strip()

def list_files(path, target_extensions):
    for root, dirs, files in os.walk(path):
        for file in files:
            for extension in target_extensions:
                if file.endswith(extension):
                    yield file

def main(argv):
    used_files = []
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', action='store',  dest='source_folder', help='source folder')
    parser.add_argument('-t', action='store',  dest='target_folder', help='target folder')
    parser.add_argument('-e', action='append', dest='source_extensions', default=[], help='source extensions')
    parser.add_argument('-f', action='append', dest='target_extensions', default=[], help='target extensions')

    results = parser.parse_args()

    print "source folder = "    , results.source_folder    
    print "target folder = "    , results.target_folder    
    print "source extensions = ", results.source_extensions
    print "target extensions = ", results.target_extensions

    for file, path, line_number, line in search(results.source_folder, results.target_folder, results.source_extensions, results.target_extensions):
        used_files.append(file)
        print ("{0} : {1} matches in line {2}: '{3}'".format(file, path, line_number, line))
    used_files_set = set(used_files)

    print("\n\nList of used files\n\n")

    for file in used_files_set:
        print(file)

    print("\n\nList of unused files\n\n")

    unused_files = list(set(all_files_list) - used_files_set)
    for file in unused_files:
        print(file)

if __name__ == "__main__":
    main(sys.argv[1:])    